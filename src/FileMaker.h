#pragma once
#include "BitFont.h"
#include "Charset.h"

class CFileMaker
{
public:
	static BOOL MakeCppFile(CBitFont* pBitFont, CCharset* pCharset, CFile *file, BOOL vert_scan, BOOL msb, BOOL monospace);
	static BOOL MakeBinFile(CBitFont* pBitFont, CCharset* pCharset, CFile *file, BOOL vert_scan, BOOL msb, BOOL monospace);
};
